/*
 * Trains UnifiedNLP
 * Copyright (C) 2023 Sorunome
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains;

import android.location.Location;
import android.util.Log;
import java.util.function.Consumer;


interface MultiProviderGetLocationConsumer {
	Consumer<Location> run(int i);
}

interface MultiProviderSetFunction {
	Provider[] run(MultiProviderGetLocationConsumer report);
}

public class MultiProvider extends Provider {
	private Provider[] providers;
	final private MultiProviderSetFunction getProviders;
	private boolean gotValid = false;
	public MultiProvider(Consumer<Location> report, MultiProviderSetFunction getProviders){
		super(report);
		this.getProviders = getProviders;
	}

	private void setAsValid(int i) {
		if (gotValid) {
			return;
		}
		Log.d(MultiProvider.class.getName(), "Got valid provider, stopping the other ones...");
		for (int j = 0; j < providers.length; j++) {
			if (i != j) {
				providers[j].stop();
			}
		}
		gotValid = true;
	}

	@Override
	public void start() {
		providers = getProviders.run((int i) -> {
			return l -> {
				setAsValid(i);
				report(l);
			};
		});
		Log.d(MultiProvider.class.getName(), "Starting providers...");
		for (Provider p : providers) {
			p.start();
		}
	}

	@Override
	public void stop() {
		Log.d(MultiProvider.class.getName(), "Stopping providers...");
		for (Provider p : providers) {
			p.stop();
		}
	}
}
