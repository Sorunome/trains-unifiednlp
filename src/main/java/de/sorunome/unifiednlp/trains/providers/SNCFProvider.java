/*
 * Trains UnifiedNLP
 * Copyright (C) 2022 VxlerieUwU
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * SNCF TGV InOui wifi
 * by VxlerieUwU
 */

package de.sorunome.unifiednlp.trains.providers;

import android.location.Location;
import android.util.Log;
import de.sorunome.unifiednlp.trains.IntervalProvider;
import de.sorunome.unifiednlp.trains.Utils;
import java.util.function.Consumer;
import java.lang.StringBuffer;
import org.json.JSONObject;
import org.microg.nlp.api.LocationHelper;

public class SNCFProvider extends IntervalProvider {
	private String domainName;
	private String url;

	public SNCFProvider(Consumer<Location> report, String domainName) {
		super(report);
		this.domainName = domainName;
	}

	private static final String TAG = SNCFProvider.class.getName();

	@Override
	protected void setup() {
		Log.d(TAG, "Got SNCF wifi " + domainName);
	}

	@Override
	protected void callback() {
		if (domainName != "OUIGO") {
			url =  new StringBuffer("https://").append(domainName).append("/router/api/train/gps").toString();
		} else {
			url = "https://ouifi.ouigo.com:8084/api/gps";
		}

		try {
			// {"success":true,"fix":10,"timestamp":1661185032,"latitude":48.0000000,"longitude":8.0000000,"altitude":119.34,"speed":34.005,"heading":226.9}
			JSONObject response = new JSONObject(Utils.getUrl(url));
			Log.d(TAG, "Response: " + response);
			Location location = LocationHelper.create("trains");
			location.setLatitude(response.getDouble("latitude"));
			location.setLongitude(response.getDouble("longitude"));
			location.setAccuracy(response.getInt("fix"));
			if (!response.isNull("altitude")) {
				Utils.setAltitudeMeters(location, response.getDouble("altitude"));
			}
			if (!response.isNull("speed")) {
				location.setSpeed((float) (response.getDouble("speed")));
			}

			if (!response.isNull("heading")) {
				location.setBearing((float) (response.getDouble("heading")));
			}


			Log.d(TAG, "Just reported: " + location);
			report(location);
		} catch (Exception e) {
			report(null);
			Log.w(TAG, "Caught exception " + e.toString());
		}
	}
}
