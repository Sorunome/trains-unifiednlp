/*
 * Trains UnifiedNLP
 * Copyright (C) 2022, 2023 Sorunome
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains.providers;

import android.location.Location;
import android.util.Log;
import de.sorunome.unifiednlp.trains.IntervalProvider;
import de.sorunome.unifiednlp.trains.Utils;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.function.Consumer;
import org.json.JSONException;
import org.json.JSONObject;
import org.microg.nlp.api.LocationHelper;

public class PassengeraProvider extends IntervalProvider {
	private String endpoint;
	private String name;

	public PassengeraProvider(Consumer<Location> report, String endpoint, String name) {
		super(report);
		this.endpoint = endpoint;
		this.name = name;
	}

	private static final String TAG = PassengeraProvider.class.getName();

	@Override
	protected void setup() {
		Log.d(TAG, "Got " + name + " wifi");
		autodetectBadGps = true;
	}

	@Override
	protected void callback() {
		try {
			// {"gpsLat":49.982498300000003,"gpsLng":12.6498176,"prevGpsLat":49.9837007,"prevGpsLng":12.6490516,"speed":100,"delay":10,"altitude":592.20000000000005,"temperature":null}
			JSONObject response = new JSONObject(Utils.getUrl(endpoint + "/api/vehicle/realtime"));
			Log.d(TAG, "Response: " + response);
			Location location = LocationHelper.create("trains");
			location.setLatitude(response.getDouble("gpsLat"));
			location.setLongitude(response.getDouble("gpsLng"));
			location.setAccuracy(50.0f);
			if (!response.isNull("altitude")) {
				Utils.setAltitudeMeters(location, response.getDouble("altitude"));
			}
			if (!response.isNull("speed")) {
				location.setSpeed((float) (response.getDouble("speed") * 10.0 / 36.0));
			}

			Log.d(TAG, "Just reported: " + location);
			report(location);
		} catch (Exception e) {
			report(null);
			Log.w(TAG, "Caught exception " + e.toString());
		}
	}
}
