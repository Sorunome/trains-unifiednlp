/*
 * Trains UnifiedNLP
 * Copyright (C) 2023 Sorunome
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains.providers;

import android.location.Location;
import android.util.Log;
import de.sorunome.unifiednlp.trains.IntervalProvider;
import de.sorunome.unifiednlp.trains.Utils;
import java.net.UnknownHostException;
import java.util.function.Consumer;
import org.json.JSONObject;
import org.microg.nlp.api.LocationHelper;

public class OEBBProvider extends IntervalProvider {
	public OEBBProvider(Consumer<Location> report) {
		super(report);
	}

	private static final String TAG = OEBBProvider.class.getName();

	@Override
	protected void setup() {
		Log.d(TAG, "Got ÖBB wifi");
		delay = 2000;
		autodetectBadGps = true;
	}

	@Override
	protected void callback() {
		try {
			JSONObject response = new JSONObject(Utils.getUrl("https://railnet.oebb.at/api/gps"));
			Location location = LocationHelper.create("trains");
			location.setLatitude(Utils.getJsonDouble(response, "Latitude"));
			location.setLongitude(Utils.getJsonDouble(response, "Longitude"));
			location.setAccuracy(50.0f);
			try {
				double kmh = Double.parseDouble(Utils.getUrl("https://railnet.oebb.at/api/speed"));
				location.setSpeed((float) (kmh * 10.0 / 36.0));
			} catch (Exception e) { }

			Log.d(TAG, "Just reported: " + location);
			report(location);
		} catch (Exception e) {
			report(null);
			Log.w(TAG, "Caught exception " + e.toString());
		}
	}
}
