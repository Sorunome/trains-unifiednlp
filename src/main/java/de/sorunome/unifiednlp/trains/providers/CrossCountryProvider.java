/*
 * Trains UnifiedNLP
 * Copyright (C) 2023 Sorunome
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains.providers;

import android.location.Location;
import android.util.Log;
import de.sorunome.unifiednlp.trains.IntervalProvider;
import de.sorunome.unifiednlp.trains.Utils;
import java.util.function.Consumer;
import org.json.JSONObject;
import org.microg.nlp.api.LocationHelper;

public class CrossCountryProvider extends IntervalProvider {
	public CrossCountryProvider(Consumer<Location> report) {
		super(report);
	}

	private static final String TAG = CrossCountryProvider.class.getName();

	@Override
	protected void setup() {
		Log.d(TAG, "Got Cross Country wifi");
		autodetectBadGps = false;
		delay = 2000;
	}

	@Override
	protected void callback() {
		try {
			// {"Latitude":"53.183021666667","Longitude":"-1.4007583333333", "JSON":{"error":"none","lat":53.183021666667,"lon":-1.4007583333333,"speed":76.1,"bearing":185.5,"sats":"under-construction"}}
			JSONObject response = new JSONObject(Utils.getUrl("http://crosscountrywifi.co.uk/api/gps")).getJSONObject("JSON");
			Log.d(TAG, "Response: " + response);
			if (!response.getString("error").equals("none")) {
				Log.d(TAG, "GPS position lost");
				report(null);
				return;
			}
			Location location = LocationHelper.create("trains");
			location.setLatitude(response.getDouble("lat"));
			location.setLongitude(response.getDouble("lon"));
			location.setAccuracy(50.0f);
			if (!response.isNull("speed")) {
				// is in miles per hour
				location.setSpeed((float) (response.getDouble("speed") / 2.2369362920544));
			}
			if (response.getDouble("bearing") != -1) {
				location.setBearing((float) response.getDouble("bearing"));
			}

			Log.d(TAG, "Just reported: " + location);
			report(location);
		} catch (Exception e) {
			report(null);
			Log.w(TAG, "Caught exception " + e.toString());
		}
	}
}
