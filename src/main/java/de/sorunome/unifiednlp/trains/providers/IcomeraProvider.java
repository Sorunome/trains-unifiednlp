/*
 * Trains UnifiedNLP
 * Copyright (C) 2023 Sorunome
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains.providers;

import android.location.Location;
import android.util.Log;
import de.sorunome.unifiednlp.trains.IntervalProvider;
import de.sorunome.unifiednlp.trains.Utils;
import java.util.function.Consumer;
import org.json.JSONObject;
import org.microg.nlp.api.LocationHelper;

public class IcomeraProvider extends IntervalProvider {
	private String name;

	public IcomeraProvider(Consumer<Location> report, String name) {
		super(report);
		this.name = name;
	}

	private static final String TAG = IcomeraProvider.class.getName();

	@Override
	protected void setup() {
		Log.d(TAG, "Got " + name + " (Icomera) wifi");
		delay = 2000;
		autodetectBadGps = true;
	}

	@Override
	protected void callback() {
		try {
			String rawResponse;
			try {
				rawResponse = Utils.getUrl("https://www.ombord.info/api/jsonp/position/");
			} catch (Exception e) {
				rawResponse = Utils.getUrl("http://www.ombord.info/api/jsonp/position/");
			}
			JSONObject response = new JSONObject(rawResponse.replace("(", " ").replace(")", " ").replace(";", " "));
			Location location = LocationHelper.create("trains");
			location.setLatitude(Utils.getJsonDouble(response, "latitude"));
			location.setLongitude(Utils.getJsonDouble(response, "longitude"));
			location.setAccuracy(50.0f);
			try {
				Utils.setAltitudeMeters(location, Utils.getJsonDouble(response, "altitude"));
			} catch (Exception e) { }
			try {
				location.setSpeed((float) Utils.getJsonDouble(response, "speed"));
			} catch (Exception e) { }
			try {
				location.setTime((long) Utils.getJsonInt(response, "time") * 1000);
			} catch (Exception e) { }
			try {
				float bearing = (float) Utils.getJsonDouble(response, "cmg");
				if (bearing >= 0.0f) {
					location.setBearing(bearing);
				}
			} catch (Exception e) { }

			Log.d(TAG, "Just reported: " + location);
			report(location);
		} catch (Exception e) {
			report(null);
			Log.w(TAG, "Caught exception " + e.toString());
		}
	}
}
