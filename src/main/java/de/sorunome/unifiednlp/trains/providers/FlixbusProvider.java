/*
 * Trains UnifiedNLP
 * Copyright (C) 2022 VxlerieUwU
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains.providers;

import android.location.Location;
import android.util.Log;
import de.sorunome.unifiednlp.trains.IntervalProvider;
import de.sorunome.unifiednlp.trains.Utils;
import java.net.UnknownHostException;
import java.util.function.Consumer;
import org.json.JSONObject;
import org.microg.nlp.api.LocationHelper;

public class FlixbusProvider extends IntervalProvider {
    public FlixbusProvider(Consumer<Location> report) {
        super(report);
    }

    private static final String TAG = FlixbusProvider.class.getName();

    private int badWifiCounter = 0;

    @Override
    protected void setup() {
        Log.d(TAG, "Got Flixbus wifi");
        autodetectBadGps = true;
    }

    @Override
    protected void callback() {
        try {
            // {"latitude":50.006166,"longitude":19.944917,"speed":5.573}
            JSONObject response = new JSONObject(Utils.getUrl("https://media.flixbus.com/services/pis/v1/position"));
            Log.d(TAG, "Response: " + response);
            Location location = LocationHelper.create("trains");
            location.setLatitude(response.getDouble("latitude"));
            location.setLongitude(response.getDouble("longitude"));
            location.setAccuracy(50.0f);

            /*if (!response.isNull("altitude")) {
                Utils.setAltitudeMeters(location, response.getDouble("altitude"));

            }*/

            if (!response.isNull("speed")) {
                location.setSpeed((float) (response.getDouble("speed")));
            }

            badWifiCounter = 0;

            Log.d(TAG, "Just reported: " + location);
            report(location);
        } catch (UnknownHostException e) {
            // when just connecting to the wifi the network might incorrectly not
            // send the A record yet
            badWifiCounter++;
            if (badWifiCounter > 3) {
                Log.d(TAG, "Portal does not serve location data, stopping");
                stop();
            }
        } catch (Exception e) {
            report(null);
            Log.w(TAG, "Caught exception " + e.toString());
        }
    }
}
