#!/bin/bash
pandoc -f markdown -t html -o fastlane/metadata/android/en-US/full_description.txt -L md-to-html-fastlane-filter.lua README.md
# remove "get on fdroid" button
sed -i -E -n '1h;2,$H;${g;s~<p>[^>]*>[^>]*https://fdroid.gitlab.io/artwork/badge/get-it-on.png[^>]*>[^>]*></p>~~ig;p}' fastlane/metadata/android/en-US/full_description.txt
