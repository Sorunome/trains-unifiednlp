# Trains UnifiedNLP

This is a UnifiedNLP provider that detects if you are connected to a trains wifi and, if so, provides the location data provided by the train to your android phone.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/de.sorunome.unifiednlp.trains/)

## Currently supported trains
 - 🇦🇹 ÖBB
 - 🇧🇪 Thalys
 - 🇨🇿 České Dráhy (InterJet, Pendolino, some IC)
 - 🇫🇷️ SNCF (TGV InOui, Intercités, Train NOMAD)
 - 🇩🇪 Deutsche Bahn (ICE, some IC, some regional trains)
 - 🇭🇺 Magyar Államvasutak
 - 🇳🇱 Keolis
 - 🇬🇧 CrossCountry, GWR
 - 🇪🇺 FlixBus
